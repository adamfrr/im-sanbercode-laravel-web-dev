<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First name:</label><br><br>
        <input type="text" id="firstname" name="firstname"><br><br>
        <label for="lastname">Last name:</label><br><br>
        <input type="text" id="lastname" name="lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="1">Male <br>
        <input type="radio" name="gender" value="2">Female <br>
        <input type="radio" name="gender" value="3">Other <br><br>
        <label for="nationality">Nationality</label><br><br>
        <select name="nationality"><br>
            <option value="1">Indonesian</option>
            <option value="2">Inggris</option>
            <option value="3">German</option>
            <option value="4">Spanyol</option>
            <option value="5">Other</option>
        </select><br><br>
        <label for="languagespoken">Language Spoken:</label><br><br>
        <input type="checkbox" name="languagespoken" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="languagespoken" value="2">English <br>
        <input type="checkbox" name="languagespoken" value="3">Arabic <br>
        <input type="checkbox" name="languagespoken" value="4">Other <br><br>
        <label for="bio">Bio</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">


    </form>
</body>
</html>