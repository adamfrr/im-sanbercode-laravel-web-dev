<?php
require_once ('Animal.php');
require_once ('Frog.php');
require_once ('Ape.php');


$sheep = new Animal("shaun");

echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "Legs : ".$sheep->legs."<br>"; // 4
echo "Cold Blooded : ".$sheep->cold_blooded."<br>"; // "no"

echo"----------------------<br>";

$kodok = new Frog("buduk");

echo "Name : ".$kodok->name."<br>";
echo "Legs : ".$kodok->legs."<br>";
echo "Cold Blooded : ".$kodok->cold_blooded."<br>";
$kodok->jump();

echo"----------------------<br>";

$sungokong = new Ape("Kera Sakti");

echo "Name : ".$sungokong->name."<br>";
echo "Legs : ".$sungokong->legs."<br>";
echo "cold Blooded : ".$sungokong->cold_blooded."<br>";
$sungokong->yell();

?>