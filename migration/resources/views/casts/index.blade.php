@extends('layout.master')

@section('judul')
    casts
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm">Tambah Data</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casts as $key => $cast)
        <tr>
            <th>{{ $key +1 }}</th>
            <td>{{ $cast->nama }}</td>
            <td>{{ $cast->umur }}</td>
            <td>
                <form action="/cast/{{$cast->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$cast->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="DELETE">
                </form>
            </td>
            
          </tr>      
        @empty
            <p>No users</p>
        @endforelse
      
    </tbody>
  </table>
@endsection