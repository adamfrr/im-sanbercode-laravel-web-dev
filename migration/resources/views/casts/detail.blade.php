@extends('layout.master')

@section('judul')
    Cast Detail
@endsection

@section('content')
    <h2>Nama: {{ $casts->nama }}</h2>
    <h2>Umur: {{ $casts->umur }}</h2>
    <p>Bio: {{ $casts->bio }}</p>
    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection