@extends('layout.master')

@section('judul')
    casts edit
@endsection

@section('content')
    <h1>Ini Adalah Halaman edit cast</h1>
    <form action="/cast/{{$casts->id}}" method="POST">
        @csrf
        @method("PUT")
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" placeholder="Enter name" name="nama" value="{{ $casts->nama }}">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror   
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="integer" class="form-control" id="umur" placeholder="Enter umur" name="umur" value="{{ $casts->umur }}">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="bio">Bio</label>
          <textarea class="form-control" id="bio" rows="3" name="bio">{{ $casts->bio }}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>    
@endsection