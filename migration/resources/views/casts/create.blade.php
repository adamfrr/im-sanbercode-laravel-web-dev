@extends('layout.master')

@section('judul')
    casts|create
@endsection

@section('content')
    <h1>Ini Adalah Halaman create</h1>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" placeholder="Enter name" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror   
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="integer" class="form-control" id="umur" placeholder="Enter umur" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="bio">Bio</label>
          <textarea class="form-control" id="bio" rows="3" name="bio"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>    
@endsection