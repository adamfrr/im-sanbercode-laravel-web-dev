<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('halaman.dashboard');
});

Route::get('/table',function(){
    return view('halaman.table');
});

Route::get('/data-table',function(){
    return view('halaman.data-tables');
});

// membuat tampilan tambah data
Route::get('/cast/create', [CastController::class, 'create']);
// input ke database
Route::post('/cast',[CastController::class, 'store']);
//menampilkan data keseluruhan
Route::get('/cast',[CastController::class, 'index']);
//menampilkan detail data
Route::get('/cast/{cast_id}',[CastController::class, 'show']);
//menampilkan data yang mau di edit
Route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']);
//update data yang sudah di edit ke database
Route::put('/cast/{cast_id}',[CastController::class, 'update']);
//delet data
Route::delete('/cast/{cast_id}',[CastController::class, 'destroy']);